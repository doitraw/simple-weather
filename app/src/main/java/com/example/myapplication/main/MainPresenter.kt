package com.example.myapplication.main

import com.example.myapplication.main.data.WeatherDataRepository
import io.reactivex.Scheduler

class MainPresenter(
    private val weatherDataRepository: WeatherDataRepository,
    private val mainThreadScheduler: Scheduler,
    private val backgroundThreadScheduler: Scheduler
) : MainContract.Presenter() {

    override fun onLoadData() {
        disposable = weatherDataRepository.getForecast()
            .subscribeOn(backgroundThreadScheduler)
            .observeOn(mainThreadScheduler)
            .subscribe(
                { view?.showData(it) },
                { showError() },
                { showDataLoaded() }
            )
    }

    override fun onRefreshData() {
        disposable = weatherDataRepository.getForecast()
            .subscribeOn(backgroundThreadScheduler)
            .observeOn(mainThreadScheduler)
            .subscribe(
                { view?.showData(it) },
                { showError() },
                { showDataLoaded() }
            )
    }

    override fun detachView() {
        disposable.dispose()
        super.detachView()
    }

    private fun showError() {
        view?.apply {
            showLoading(false)
            showRefreshing(false)
            showData(listOf())
            showError(true)
        }
    }

    private fun showDataLoaded() {
        view?.apply {
            showLoading(false)
            showRefreshing(false)
            showError(false)
        }
    }
}