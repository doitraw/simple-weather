package com.example.myapplication.main

import com.example.myapplication.app.base.BasePresenter
import com.example.myapplication.app.base.BaseView
import com.example.myapplication.main.view.model.DailyForecast

interface MainContract {
    interface View : BaseView {
        fun showData(forecasts: List<DailyForecast>)
        fun showRefreshing(show: Boolean)
        fun showLoading(show: Boolean)
        fun showError(show: Boolean)
    }

    abstract class Presenter : BasePresenter<View>() {
        abstract fun onLoadData()
        abstract fun onRefreshData()
    }
}