package com.example.myapplication.main.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.myapplication.R
import com.example.myapplication.app.extensions.changeVisibility
import com.example.myapplication.main.MainContract
import com.example.myapplication.main.view.adapter.DailyForecastAdapter
import com.example.myapplication.main.view.model.DailyForecast
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), MainContract.View {

    @Inject
    lateinit var presenter: MainContract.Presenter
    @Inject
    lateinit var adapter: DailyForecastAdapter

    //region lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.apply {
            attachView(this@MainActivity)
            onLoadData()
        }
        swipeRefreshLayout.setOnRefreshListener {
            presenter.onRefreshData()
        }
        forecastsRV.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = this@MainActivity.adapter
        }
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    //endregion

    //region contract

    override fun showData(forecasts: List<DailyForecast>) {
        adapter.setItems(forecasts)
    }

    override fun showLoading(show: Boolean) {
        progressBar.changeVisibility(show)
    }

    override fun showRefreshing(show: Boolean) {
        swipeRefreshLayout.isRefreshing = show
    }

    override fun showError(show: Boolean) {
        errorTV.changeVisibility(show)
    }

    //end region

}
