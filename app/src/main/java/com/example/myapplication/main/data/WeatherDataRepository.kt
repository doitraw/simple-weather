package com.example.myapplication.main.data

import com.example.myapplication.main.view.model.DailyForecast
import io.reactivex.Observable

interface WeatherDataRepository {

    /*
        523920 is Warsaw's id. There's a lot of places this could have been kept,
        but since it's supposed to be an MVP we're keeping it here.
     */
    fun getForecast(id: String = "523920"): Observable<List<DailyForecast>>
}
