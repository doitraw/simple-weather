package com.example.myapplication.main.data

import com.example.myapplication.app.api.WeatherApi
import com.example.myapplication.app.extensions.toPresentationModel
import com.example.myapplication.main.view.model.DailyForecast
import io.reactivex.Observable

class WeatherDataApiRepository(private val weatherApi: WeatherApi) : WeatherDataRepository {
    override fun getForecast(id: String): Observable<List<DailyForecast>> = weatherApi
        .getForecastById(id)
        .map { getForecast ->
            getForecast.consolidatedWeatherList.map { it.toPresentationModel() }
        }
}