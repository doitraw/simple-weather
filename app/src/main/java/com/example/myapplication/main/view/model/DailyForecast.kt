package com.example.myapplication.main.view.model

data class DailyForecast(
    val date: String,
    val maxTemp: Int,
    val temp: Int,
    val minTemp: Int,
    val weatherStateIconUrl: String
)