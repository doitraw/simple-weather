package com.example.myapplication.main.di

import com.example.myapplication.app.di.BackgroundThreadScheduler
import com.example.myapplication.app.di.MainThreadScheduler
import com.example.myapplication.main.MainContract
import com.example.myapplication.main.MainPresenter
import com.example.myapplication.main.data.WeatherDataRepository
import com.example.myapplication.main.view.adapter.DailyForecastAdapter
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler

@Module
class MainModule {

    @Provides
    fun provideMainPresenter(
        weatherDataRepository: WeatherDataRepository,
        @MainThreadScheduler mainThreadScheduler: Scheduler,
        @BackgroundThreadScheduler backgroundThreadScheduler: Scheduler
    ): MainContract.Presenter =
        MainPresenter(
            weatherDataRepository,
            mainThreadScheduler,
            backgroundThreadScheduler
        )

    @Provides
    fun provideDailyForecastAdapter(): DailyForecastAdapter = DailyForecastAdapter()

}
