package com.example.myapplication.main.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.myapplication.R
import com.example.myapplication.app.extensions.dateToDay
import com.example.myapplication.app.extensions.toDegreesString
import com.example.myapplication.main.view.model.DailyForecast
import kotlinx.android.synthetic.main.item_forecast.view.*

class DailyForecastAdapter : RecyclerView.Adapter<DailyForecastAdapter.ViewHolder>() {

    private val dailyForecasts: MutableList<DailyForecast> = mutableListOf()

    fun setItems(forecasts: List<DailyForecast>) {
        dailyForecasts.apply {
            clear()
            addAll(forecasts)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, index: Int) = ViewHolder(
        LayoutInflater.from(viewGroup.context).inflate(
            R.layout.item_forecast,
            viewGroup,
            false
        )
    )

    override fun getItemCount() = dailyForecasts.size

    override fun onBindViewHolder(viewHolder: ViewHolder, index: Int) {
        viewHolder.bind(dailyForecasts[index])
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        internal fun bind(dailyForecast: DailyForecast) {
            view.apply {
                nameTV.text = dailyForecast.date.dateToDay()
                minTempTV.text = context.getString(R.string.min_temp, dailyForecast.minTemp.toDegreesString())
                maxTempTV.text = context.getString(R.string.max_temp, dailyForecast.maxTemp.toDegreesString())
                tempTv.text = dailyForecast.temp.toDegreesString()
                Glide.with(this).load(dailyForecast.weatherStateIconUrl).into(weatherIconIV)
            }
        }
    }
}
