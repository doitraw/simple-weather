package com.example.myapplication.app.base

import io.reactivex.disposables.Disposable

abstract class BasePresenter<T : BaseView> {
    protected var view: T? = null
    protected lateinit var disposable: Disposable
    open fun attachView(view: T) {
        this.view = view
    }

    open fun detachView() {
        this.view = null
    }
}