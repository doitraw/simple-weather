package com.example.myapplication.app.di

import com.example.myapplication.main.view.MainActivity
import com.example.myapplication.main.di.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainModule::class])
    internal abstract fun bindMainActivity(): MainActivity

}