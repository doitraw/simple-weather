package com.example.myapplication.app.api

import com.example.myapplication.app.api.model.GetForecast
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface WeatherApi {
    @GET("location/{id}/")
    fun getForecastById(@Path("id") id: String): Observable<GetForecast>
}
