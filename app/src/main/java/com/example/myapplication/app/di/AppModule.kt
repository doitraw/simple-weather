package com.example.myapplication.app.di

import com.example.myapplication.app.api.WeatherApi
import com.example.myapplication.main.data.WeatherDataApiRepository
import com.example.myapplication.main.data.WeatherDataRepository
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideWeatherApi(retrofit: Retrofit): WeatherApi = retrofit.create(WeatherApi::class.java)

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl("https://www.metaweather.com/api/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    @Provides
    @Singleton
    fun provideWeatherDataRepository(weatherApi: WeatherApi): WeatherDataRepository =
        WeatherDataApiRepository(weatherApi)

    @Provides
    @Singleton
    @MainThreadScheduler
    fun provideMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @Singleton
    @BackgroundThreadScheduler
    fun provideBackgroundThreadScheduler(): Scheduler = Schedulers.io()
}