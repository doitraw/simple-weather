package com.example.myapplication.app.extensions

import android.view.View


fun View.changeVisibility(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}
