package com.example.myapplication.app.extensions

import java.text.ParseException
import java.text.SimpleDateFormat

fun String.dateToDay(): String = try {
    SimpleDateFormat("EEEE").format(SimpleDateFormat("yyyy-MM-dd").parse(this))
} catch (e: ParseException) {
    this
}

fun Int.toDegreesString(): String = "$this${Typography.degree}"
