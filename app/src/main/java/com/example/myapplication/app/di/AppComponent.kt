package com.example.myapplication.app.di

import com.example.myapplication.app.SimpleWeatherApp
import com.example.myapplication.main.di.MainModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilder::class,
        AppModule::class,
        MainModule::class
    ]
)
interface AppComponent : AndroidInjector<SimpleWeatherApp> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<SimpleWeatherApp>()
}