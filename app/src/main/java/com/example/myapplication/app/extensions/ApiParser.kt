package com.example.myapplication.app.extensions

import com.example.myapplication.app.api.model.ConsolidatedWeather
import com.example.myapplication.main.view.model.DailyForecast
import kotlin.math.roundToInt

fun ConsolidatedWeather.toPresentationModel() = DailyForecast(
    date = applicableDate,
    maxTemp = maxTemp.roundToInt(),
    temp = theTemp.roundToInt(),
    minTemp = minTemp.roundToInt(),
    weatherStateIconUrl = "https://www.metaweather.com/static/img/weather/png/$weatherStateAbbr.png"
)

