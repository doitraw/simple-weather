package com.example.myapplication.main

import com.example.myapplication.main.data.WeatherDataRepository
import com.example.myapplication.main.view.model.DailyForecast
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import java.io.IOException

class MainPresenterTest {
    private val view: MainContract.View = mock()
    private val weatherDataRepository: WeatherDataRepository = mock()
    private val scheduler = TestScheduler()

    private val presenter: MainContract.Presenter = MainPresenter(weatherDataRepository, scheduler, scheduler)

    @Before
    fun setup() {
        presenter.attachView(view)
    }

    @Test
    fun `onLoadData - shows data and hides all other views on successful response`() {
        val mockList = listOf<DailyForecast>(mock(), mock())
        Mockito.`when`(weatherDataRepository.getForecast()).thenReturn(
            Observable.just(mockList) // returning empty list, but still a successful response
        )
        presenter.onLoadData()
        scheduler.triggerActions()
        verify(view).apply {
            showData(eq(mockList))
            showLoading(false)
            showRefreshing(false)
            showError(false)
        }
    }

    @Test
    fun `onLoadData - shows error and hides all other views on error response`() {
        Mockito.`when`(weatherDataRepository.getForecast()).thenReturn(
            Observable.error(IOException())
        )
        presenter.onLoadData()
        scheduler.triggerActions()
        verify(view).apply {
            showData(eq(listOf()))
            showLoading(false)
            showRefreshing(false)
            showError(false)
        }
    }

    @Test
    fun `onRefreshData() - shows data and hides all other views on successful response`() {
        val mockList = listOf<DailyForecast>(mock(), mock())
        Mockito.`when`(weatherDataRepository.getForecast()).thenReturn(
            Observable.just(mockList) // returning empty list, but still a successful response
        )
        presenter.onRefreshData()
        scheduler.triggerActions()
        verify(view).apply {
            showData(eq(mockList))
            showLoading(false)
            showRefreshing(false)
            showError(false)
        }
    }

    @Test
    fun `onRefreshData() - shows error and hides all other views on error response`() {
        Mockito.`when`(weatherDataRepository.getForecast()).thenReturn(
            Observable.error(IOException())
        )
        presenter.onRefreshData()
        scheduler.triggerActions()
        verify(view).apply {
            showData(eq(listOf()))
            showLoading(false)
            showRefreshing(false)
            showError(false)
        }
    }


}